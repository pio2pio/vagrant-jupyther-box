# Synopsis
This is Jupyther Notebook vagrant box.

# Provision
Run ```vagrant up``` to provision this box. On average it takes 2m30s to complete.
```sh
time vagrant up

real	4m32.088s
user	0m12.911s
sys	0m4.555s
```

# Destroy vm
```sh
vagrant destroy -f
```

# Usage
Once provisioned sucessfully Jupyther Notebook is available at http://localhost:8888 both on a host machine and within GuestVM. The modern text based web browser ``browsh`` has been installed so you can simply access from fingertips of your terminal.
```sh
vagrant ssh
browsh http://localhost:8888
```

# Details
This box is based on Ubuntu 18 LTS. Where tools like ```tree, tmux, browsh``` get installed. ```Jupyther Notebook``` gets installed in ```vagrant``` user mode within ```python3``` virtual environment. Then ```systemd``` ```jupyter.service``` gets created that automatically starts up and listens on port 8888.

[Theme manager](https://github.com/dunovank/jupyter-themes) ```jupyterthemes```  for dark-mode gets installed.

[Widgets](https://ipywidgets.readthedocs.io/en/latest/index.html) package ```ipywidgets``` gets installed.

Within ```script_install_jupyter``` script you find additional but not essential Python packages like ```matpltlib, pandas```. You can keep adding to the list to enhance Notebook functionality.

# Reusage
```Vagrantfile``` has been reused from my another project thus sections like disksize, docker (disabled), gui (disabled) are in the code. This allows flexible in line configurations if needed.

# Troubleshoting
Functionality can be easly enhanced by adding new packages, editing startup scripts etc. For this reason sometimes you may want to restart ```jupyter.service``` and watch logs.
```sh
host$> vagrant ssh

guest$> sudo systemctl daemon-reload && sudo systemctl restart jupyter.service; sudo journalctl -u jupyter.service -f

guest$> source jupyter/bin/activate
```