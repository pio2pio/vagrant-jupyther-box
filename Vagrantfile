# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.require_version ">= 2.0.0"

#---------------------------------------
# Inline scripts
#---------------------------------------
$script_update_os_packages = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\e[m' "INFO running script_update_os_packages script"
export DEBIAN_FRONTEND=noninteractive
apt-get update && apt-get -qq upgrade
SCRIPT

$script_install_common_tools = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_install_common_tools script"
export DEBIAN_FRONTEND=noninteractive

# System locales
localectl set-locale LANG=en_GB.UTF-8 LANGUAGE=en_GB:en; localectl set-keymap gb; localectl set-x11-keymap gb

# Clean up motd
rm -f /etc/update-motd.d/{*help*,*landscape*,*news*,*cloudguest*,*livepatch*,*eol*}

# Packages
apt-get -qq install tmux tree
SCRIPT

$script_add_gui = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running add_gui script"
export DEBIAN_FRONTEND=noninteractive
# GUI mode only packages
apt-get -qq install -y ubuntu-desktop --no-install-recommends
apt-get -qq install -y terminator
apt-get -qq install -y x11-xkb-utils

# Optional
apt-get -qq --no-install-recommends install -y firefox chromium-browser

# Commented out as causing err Cannot open display "default display"
# printf "Your env variable DISPLAY=$DISPLAY"
# setxkbmap gb # causes: Cannot open display "default display"
SCRIPT

$script_install_docker = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_install_docker script"
export DEBIAN_FRONTEND=noninteractive
# apt-get -qq remove docker docker-engine docker.io containerd runc docker-ce  # start fresh
apt-get -qq install apt-transport-https ca-certificates curl gnupg-agent software-properties-common # apt over https
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" # or {edge|test}
apt-cache -qq madison docker-ce docker-ce-cli
apt-get   -qq install docker-ce docker-ce-cli containerd.io --allow-change-held-packages
apt-mark      hold    docker-ce docker-ce-cli containerd.io
usermod -aG docker vagrant
SCRIPT

$script_install_browsh = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_install_browsh script - Text web browser"
export DEBIAN_FRONTEND=noninteractive
apt-get -qq --no-install-recommends install firefox
VERSION=1.6.4
wget --quiet https://github.com/browsh-org/browsh/releases/download/v${VERSION}/browsh_${VERSION}_linux_amd64.deb \
     -O /tmp/browsh_${VERSION}_linux_amd64.deb
dpkg -i /tmp/browsh_${VERSION}_linux_amd64.deb
SCRIPT

$script_install_python = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_install_python script"
export DEBIAN_FRONTEND=noninteractive
apt-get -qq install python3-venv # required on Debians for ensurepip to exist
SCRIPT

$script_install_jupyter = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_install_jupyther script"
python3 -m venv jupyter
source ./jupyter/bin/activate
pip install jupyterlab notebook 
pip install ipywidgets        # Jupyter Widgets
pip install matplotlib pandas # python libs

# Run
# jupyter notebook --port=8888 --no-browser --ip=0.0.0.0 --NotebookApp.token= --notebook-dir=/home/vagrant/data
  # --ip=0.0.0.0 required for Vagrant port-forwarding
  # --NotebookApp.token= no token required
SCRIPT

$script_create__jupyter_service = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_create__jupyter_service script"

printf '\e[1;34m%-6s\n\e[m' "INFO create Jupyter Notebook service"
cat >/etc/systemd/system/jupyter.service <<EOL
[Unit]
Description=Jupyter Notebook
[Service]
Type=simple
PIDFile=/run/jupyter.pid
# Include venv bin/ so can call eg. pip from Notebook without full path
Environment="PATH=/home/vagrant/jupyter/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
ExecStart=/home/vagrant/jupyter/bin/jupyter notebook --port=8888 --no-browser --ip=0.0.0.0 --NotebookApp.token= --notebook-dir=/vagrant/data
User=vagrant
Group=vagrant
WorkingDirectory=/vagrant/data
Restart=always
RestartSec=10
[Install]
WantedBy=multi-user.target
EOL

systemctl enable jupyter.service
systemctl daemon-reload
systemctl restart jupyter.service
SCRIPT

$script_jupyter_darkmode = <<-SCRIPT
#!/bin/bash
printf '\e[1;34m%-6s\n\e[m' "INFO running script_jupyter_darkmode script"

source ./jupyter/bin/activate
pip install jupyterthemes

# Notebook theme
THEME=onedork
jt -t $THEME -T -N -kl

# Plotting theme
THEME=monokai
mkdir ~/.ipython/profile_default/startup -p
cat > ~/.ipython/profile_default/startup/00_startup.py <<EOF
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from jupyterthemes import jtplot
jtplot.style(theme='$THEME', context='notebook', ticks=True, grid=False)
EOF

printf '\e[1;34m%-6s\n\e[m' "INFO restart jupyter.service"
sudo systemctl restart jupyter.service
SCRIPT

#---------------------------------------
# Plugins install
#---------------------------------------
  required_plugins      = %w(vagrant-vbguest vagrant-disksize)
  required_plugins.each do |plugin|
    need_restart = false
    unless Vagrant.has_plugin? plugin
      system "vagrant plugin install #{plugin}"
      need_restart = true
    end
    exec "vagrant #{ARGV.join(' ')}" if need_restart
  end

#---------------------------------------
# Configure
#---------------------------------------
Vagrant.configure("2") do |config|
  machineName = File.basename(Dir.pwd)      # sets Vagrant name, VirtualBox name and hostname

  #---------------------------------------
  # VM guest config definition (multi-machine)
  #---------------------------------------
  config.vm.define machineName do |host|    # this sets up Vagrant name in 'vagrant status'
    host.vm.hostname         = machineName
    host.vm.box              = "ubuntu/bionic64" # "ubuntu/xenial64" "bento/ubuntu-18.04" "ubuntu/bionic64" aka 18.04"
    host.vm.box_check_update = false             # if false, check for updates with `vagrant box outdated`

    host.disksize.size       = '20GB' # creates only on creation, it will be .vdi

    host.vm.provider "virtualbox" do |vb|
        vb.gui    = false                           # false - headless mode, can be changed at any time
        vb.memory = "2048"                          # true  - will start Virtualbox gui, but you can double
        vb.name   = machineName + "_vagrant"        #         click on the VM name to start that anyway
    end

    #---------------------------------------
    # Networking
    #---------------------------------------
    host.vm.usable_port_range = 8888..8898
    host.vm.network "forwarded_port", guest: 8888, host: 8888, auto_correct: true, id: 'jupyter_notebook'

    #---------------------------------------
    # Provisioners
    #---------------------------------------
    host.vm.provision "shell", inline: $script_update_os_packages
    host.vm.provision "shell", inline: $script_install_common_tools
    #host.vm.provision "shell", inline: $script_install_docker
    host.vm.provision "shell", inline: $script_install_browsh
    host.vm.provision "shell", inline: $script_install_python
    host.vm.provision "shell", inline: $script_install_jupyter,  privileged: false
    host.vm.provision "shell", inline: $script_create__jupyter_service
    host.vm.provision "shell", inline: $script_jupyter_darkmode, privileged: false
    #host.vm.provision "shell", inline: $script_add_gui, reboot: true


    #---------------------------------------
    # Post provision
    #---------------------------------------
    host.trigger.after :up do |trigger|
      trigger.name = "Post provision actions"
      trigger.info = "INFO Display Jupyter forwarded port on host"
      trigger.ignore = [:destroy, :halt]
      trigger.run = {inline: "bash -c 'vagrant port --guest 8888'"}
    end

    # Welcome message
    host.vm.post_up_message = "*****************************************\n" \
                              " Access your Jupyter Notebook\n" \
                              " from a host or guest using text web browser like: browsh\n" \
                              " at http://localhost:8888\n" \
                              " run vagrant port --guest 8888 to get port on your host\n" \
                              "*****************************************"

    #---------------------------------------
    # Vbguest plugin
    #---------------------------------------
    # set auto_update=false if you don't want to install
    # guest additions matching you VirtualBox version
    if Vagrant.has_plugin?("vagrant-vbguest")
      host.vbguest.no_remote   = true
      host.vbguest.auto_update = true # false
    end
  end
end

# README.md
=begin
# Info
apt-get -qy # quiet level1 mode
apt-get -qq # quiet level2 mode has implicit -y'
--allow-change-held-packages # when packages have been marked for hold

# Issues
Error 'Cannot open display "default display"' when using gui provisioning script.
Reboot the box: 'vagrant reload'
=end
